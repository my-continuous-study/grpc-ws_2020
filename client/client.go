package main

import (
	"context"
	"fmt"
	"google.golang.org/grpc"
	"log"
	greetings "../pb"
)

func main() {
	conn,err := grpc.Dial("server:1234", grpc.WithInsecure())
	if err != nil {
		log.Fatal("connection error:", err)
	}
	defer conn.Close()

	client := greetings.NewGreetingsClient(conn)
	message := &greetings.GetGreetingMessage{ Language: "jp" }
	res, err := client.GetGreetings(context.Background(), message)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("result: %s\n", res)
}