# これは何？

これはgRPCを検証するためのワークショップです。


# ファイル構成

- client (クライアントコードと、コンテナ生成用のDockerFile)
  - client.go
  - DockerFile
- server (サーバコードと、コンテナ生成用のDockerFile)
  - server.go
  - DockerFile
- go-build (goのビルドコンテナ生成用のDockerFile)
  - DockerFile
- pb (Protocol Bufferの定義ファイル)
  - greetings.proto
- MakeFile(ビルドスクリプト)
- docker-compose.yml(サーバとクライアントのコンテナを起動)


# 利用手順

1. protoc(Protocol Bufferのコンパイラ)のビルド

    `$ make docker-build`

2. protocの実行、サーバ、クライアントコードのビルド

    `$ make build`

3. サーバ、クライアントのデプロイ(docker image build)
   
    `$ make deploy`

4. 実行

     - コンテナ起動

      `docker-compose up -d`

     - コンテナが出力したログを確認

      `docker-compose logs -f`


