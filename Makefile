all: docker-build build deploy

docker-build:
		docker build -t grpc-ws/go go-build

build: build_protoc build_server build_client

build_protoc:
		docker run --rm \
			-v $(CURDIR)/pb:/pb \
			grpc-ws/go protoc -I /pb --go_out=plugins=grpc:/pb greetings.proto

		docker run --rm \
			-v $(CURDIR)/pb:/pb \
			grpc-ws/go protoc -I /pb --java_out=/pb greetings.proto

build_server: 
		docker run --rm \
			-v $(CURDIR)/pb:/pb \
			-v $(CURDIR)/server:/server \
			grpc-ws/go go build -o /server /server/server.go 

build_client:
		docker run --rm \
			-v $(CURDIR)/pb:/pb \
			-v $(CURDIR)/client:/client \
			grpc-ws/go go build -o /client /client/client.go 

deploy:
		docker build -t grpc-ws/server server
		docker build -t grpc-ws/client client
