package main

import (
	"context"
	"errors"
	"google.golang.org/grpc"
	"log"
	"net"
	greetings "../pb"

)

type GreetingsService struct {}

func (s *GreetingsService) GetGreetings(ctx context.Context, message *greetings.GetGreetingMessage) (*greetings.GreetingResponse, error) {
	switch message.Language {
	case "jp":
		return &greetings.GreetingResponse{
			Message : "おはようございます。",
		},nil
	case "en":
		return &greetings.GreetingResponse{
			Message : "Good Morning.",
		},nil
		default:
			return nil, errors.New("Undefined Language \"${message.language}\"")
		}
	}

func main() {
	port, err := net.Listen("tcp", ":1234")
	if err != nil {
		log.Println(err.Error())
		return
	}
	s := grpc.NewServer()
	greetings.RegisterGreetingsServer(s, &GreetingsService{})
	s.Serve(port)
}